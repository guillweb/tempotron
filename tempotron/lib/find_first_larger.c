long find_first_larger(double val, double *data, long length){
    long i;
    for(i=0;i<length;i++){
        if (data[i] > val)
            return(i);
    }
    return(length-1);
}