__author__ = 'root'

from brian2 import *
import numpy as np
from lib.ctype_lib import find_first_larger

class Tempotron:
    def __init__ (self,n,tau=12 * 10**-3,tau_s=4 * 10**-3,N_iterations=10**2,N_shuffle=3,al_opt=10**-1,V_thr=0.1 ,V_0=1. ,delta_V_thr=0. ,mu=0.9,verbose=0):

        self.tau = tau
        self.tau_s = tau_s
        self.tau_max = np.log(tau/tau_s) * 1/(1/tau_s - 1/tau)

        self.N_iterations = N_iterations
        self.N_shuffle = N_shuffle
        self.al_opt = al_opt

        self.w = np.random.randn(n)
        self.n = n

        self.V_thr = V_thr
        self.V_0 = V_0
        self.delta_V_thr = delta_V_thr

        self.mu = mu
        self.dw_previous = np.zeros(n)

        self.verbose = verbose

        if verbose >= 1:
            print 'Tempotron defined. V0: {0}, Vthr: {1}, mu: {2},lbda: {3}, N_shuffle: {4}, tau: {5}, tau_s: {6}, delta_V:{7}'.format(
                self.V_0,self.V_thr,self.mu,self.al_opt,self.N_shuffle,self.tau,self.tau_s,self.delta_V_thr)


    def is_error(self,V_max,pol):

        is_error = (pol >0 and V_max < self.V_thr + self.delta_V_thr ) \
                or (pol <0 and V_max > self.V_thr - self.delta_V_thr )

        return is_error


    def K_val(self,w,t_spikes,t):
            sk = self.V_0 * (t_spikes < t) * ( np.exp( -(t-t_spikes)/self.tau) - np.exp(- (t-t_spikes)/self.tau_s ))
            return sk

    def V_val(self,w,t_spikes,t):
            sk = self.V_0 * (t_spikes < t) * ( np.exp( -(t-t_spikes)/self.tau) - np.exp(- (t-t_spikes)/self.tau_s ))
            # Compute max. Complexity T
            V = np.dot(w,sk)
            return V

def create_random_dataset(N,t0,t_end,n_spikes,K):

    X = []
    for i in range(0,K):
        rd = np.random.rand(N,n_spikes)
        x = np.sort((rd*(t_end-t0) + t0 ),axis=1)
        X.append(x)

    return X


def create_coincident_dataset(N,t0,t_end,n_spikes,K):

    X = create_random_dataset(N,t0,t_end,n_spikes,K)

    for i in range(0,K):
        rd = np.random.rand(1)
        x = X[i]
        x[:,0] = (rd*(t_end-t0) + t0)
        x = np.sort(x,axis=1)
        X[i] = x


    return X



class TempotronBasic(Tempotron):
    def set_discrete_times(self,FS=10**4,tend=1,t0=0):
        self.FS = FS
        N = int(tend*FS)+1
        self.t = t0 + np.array(range(0,N)) / float(FS)



    def spikes_g(self,tau,t_spikes,t_end=-1):


        s_g = np.zeros((self.n,len(self.t)))


        for i in range(len(t_spikes)):
            for ti in t_spikes[i]:
                if t_end==-1 or ti <= t_end:
                    s_g[i,:] += (self.t >= ti) * np.exp(-(self.t - ti) / tau)

        return s_g



    def spikes_K (self,t_spikes,t_end=-1):
        return self.V_0 * (self.spikes_g(self.tau,t_spikes,t_end) - self.spikes_g(self.tau_s,t_spikes,t_end))

    def V_vect(self,w,t_spikes,t_end=-1):
            sk = self.spikes_K(t_spikes,t_end)
            # Compute max. Complexity T
            V = np.dot(w,sk)
            ind_t_thr = np.where(self.V_thr <= V)
            if len(ind_t_thr[0]) >= 1:
                ind_t_thr = ind_t_thr[0][0]
                t_thr = self.t[ind_t_thr]

                sk = self.spikes_K(t_spikes,t_thr)
                V = np.dot(w,sk)
            else:
                t_thr = -1

            ind_t_max = np.argmax(V)
            sk = self.spikes_K(t_spikes,t_thr)


            return V,sk,ind_t_max,t_thr

    def get_max_list(self,w,t_spikes,t_end=-1):
        V,sk,ind_t_max,t_thr = self.V_vect(w,t_spikes,t_end)

        n_spk =len(t_spikes)

        t_max_list = np.zeros(n_spk-1)
        V_max_list = np.zeros(n_spk-1)

        spike_sorted = np.sort(t_spikes.flatten())
        t_ind_spk = np.int_(self.FS*spike_sorted)
        for k in range(n_spk-1):
            if(t_ind_spk[k] != t_ind_spk[k+1]):
                V_max = np.max(V[t_ind_spk[k]:t_ind_spk[k+1]])
                t_max_ind = t_ind_spk[k] + np.argmax(V[t_ind_spk[k]:t_ind_spk[k+1]])
                t_max_list[k] = self.t[t_max_ind]
                V_max_list[k] = V_max

        return t_max_list,V_max_list







    def train_one_basic(self,x,y):

        pol = y*2-1

        # Compute convolution: Spike vs PSP. Complexity Txn could be nxlog T with fft
        s_k = self.spikes_K(x)
        for i in range(0,self.N_iterations):

            # Compute max. Complexity T
            V = np.dot(self.w,s_k)
            V_max = np.max(V)
            t_max_ind = np.argmax(V)


            if not(self.is_error(V_max,pol)):
                break

            # Move toward the gradient
            grad = s_k[:,t_max_ind]

            dw = pol * self.al_opt * grad
            self.w += dw


    def get_max_exact(self,x,sp_k):
        V = np.dot(self.w,sp_k)
        spike_max_ind = np.argmax(V)
        t_max = x.ravel()[spike_max_ind] + self.tau_max

        return t_max

    def train_one_exact(self,x,y):

        pol = y*2-1

        # Compute convolution: Spike vs PSP. Complexity Txn could be n x log T with fft
        s_k = self.sp_K(x,x.ravel())

        # Compute max. Complexity n_spikes x n
        t_max = self.get_max_exact(x,s_k)
        sp_k_max = self.sp_K(x,[t_max])

        for i in range(0,self.N_iterations):
            V_max = np.dot(self.w,sp_k_max)

            if not(self.is_error(V_max,pol)):
                break

            # Move toward the gradient
            grad = sp_k_max.ravel()

            dw = pol * self.al_opt * grad
            self.w += dw

    def train(self,X,Y,train_type=0):

        N = len(X)

        for k in range(0,self.N_shuffle):
            for ind_x in np.random.permutation(N):
                if train_type == 0:
                    self.train_one_exact(X[ind_x],Y[ind_x])
                elif train_type == 1:
                    self.train_one_basic(X[ind_x],Y[ind_x])




    def predict_basic(self,X):

        N = len(X)
        Y = np.zeros(N)

        for k in range(0,N):
            x = X[k]
            s_k = self.sp_K(x,x.ravel())
            t_max = self.get_max_exact(x,s_k)
            V_max = np.dot(self.w,self.sp_K(x,[t_max]))
            Y[k] = V_max > self.V_thr


        return Y







