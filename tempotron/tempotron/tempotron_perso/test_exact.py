__author__ = 'root'

from exact import *
from basic import *
import matplotlib.pyplot as plt
import random

import resource
megs = 1024
resource.setrlimit(resource.RLIMIT_AS, (megs * 1048576L, -1L))

def basic_vs_exact_error_check(tempo,X,duration=.5,re_init=True,plot_anyway=False,FS=10**5):
    tempobasic = TempotronBasic(tempo.n,tempo.tau,tempo.tau_s,tempo.N_iterations,tempo.N_shuffle,tempo.al_opt,tempo.V_thr,tempo.V_0,tempo.delta_V_thr,tempo.mu)
    tempobasic.set_discrete_times(FS,duration*2)

    if re_init:
        tempo.init_max_finder(X)

    p = len(X)

    t_error = np.zeros(p)
    V_error = np.zeros(p)
    K_error = np.zeros(p)

    is_t_max_basic_zero = np.zeros(p, dtype=bool)
    is_time_far = np.zeros(p, dtype=bool)
    should_be_the_same = is_time_far


    N_errors = 0
    mf = tempo.max_finder
    for i in range(p):
        w = tempo.w
        x = X[i]

        mf.init_sample_max_search(x,sample_id=i)
        t_max,V_max,K_max,ind_max = mf.find_max(w)

        # Compute max. Complexity T
        V,sk,t_max_ind,t_thr = tempobasic.V_vect(w,x)
        print 'Threshold: {0}'.format(t_thr)

        V_max_basic = np.max(V)
        t_max_basic = tempobasic.t[t_max_ind]
        K_max_basic = sk[:,t_max_ind]


        t_error[i] = abs(min(t_max,duration) - t_max_basic)
        V_error[i] = abs(V_max - V_max_basic)

        if len(np.shape(K_max)) == 1:
            K_error[i] = std(K_max - K_max_basic)
        else:
            K_error[i] = std(K_max - K_max_basic)

        is_t_max_basic_zero[i] = (t_max_basic == 0)
        is_time_far[i] = t_max > duration
        should_be_the_same[i] = not(is_t_max_basic_zero[i]) and not(is_time_far[i])

        error_spotted = False
        if (t_error[i] > 3* 10**-4  or V_error[i] > 3 * 10**-3 or K_error[i] > 10**-3): #and should_be_the_same[i]:
            print 'ERROR spotted:'
            error_spotted = True

            print 't error: {0}'.format(t_error[i])
            print 'V error: {0}'.format(V_error[i])
            print 'K error: {0}'.format(K_error[i])

            print 'tmax: {0}, t_max_basic: {1}'.format(t_max,t_max_basic)
            print 'V_max: {0}, V_max_basic: {1}'.format(V_max,V_max_basic)
            wh = np.where((K_max - K_max_basic)**2 > 10**-4)
            print 'times: {2}, K_max: {0}, K_max_basic: {1}'.format(K_max[wh],K_max_basic[wh],x[wh])

            N_errors += 1

        if error_spotted or plot_anyway:
            plt.plot(tempobasic.t,V)
            plt.scatter(t_max,V_max,color='r')
            plt.scatter(t_max_basic,V_max_basic,color='b')
            plt.axhline(1,color='black')
            plt.show()

            pass

    print 'Error found: {0}'.format(N_errors)
    print 'ERROR MAX when should be the same:'
    print argmax(t_error[should_be_the_same]),max(t_error[should_be_the_same])
    print argmax(V_error[should_be_the_same]),max(V_error[should_be_the_same])
    print argmax(K_error[should_be_the_same]),max(K_error[should_be_the_same])


def exact_V_error_check(tempo,X,duration=.5,re_init=True):

    if re_init:
        tempo.init_max_finder(X)

    p = len(X)

    t_error = np.zeros(p)
    V_error = np.zeros(p)
    K_error = np.zeros(p)

    is_t_max_basic_zero = np.zeros(p, dtype=bool)
    is_time_far = np.zeros(p, dtype=bool)
    should_be_the_same = is_time_far


    N_errors = 0
    mf = tempo.max_finder
    for i in range(p):
        w = tempo.w
        x = X[i]

        mf.init_sample_max_search(x,sample_id=i)
        t_max,V_max,K_max,ind_max = mf.find_max(w)


        x_f = x.flatten()
        ind_sort = np.argsort(x_f)[0:ind_max+1]
        x_f_sorted = x_f[ind_sort]
        print 'Last spike time: {0}'.format(x_f_sorted[-1])
        print 't_max: {0}'.format(t_max)


        sk = np.zeros(tempo.n)
        sk[ind_sort] = tempo.V_0 * (np.exp(- (t_max - x_f_sorted)/tempo.tau) - np.exp(- (t_max - x_f_sorted)/tempo.tau_s))
        V = np.dot(w,sk)

        V_max_basic = np.max(V)
        K_max_basic = sk

        V_error[i] = abs(V_max - V_max_basic)
        if len(np.shape(K_max)) == 1:
            K_error[i] = std(K_max - K_max_basic)
        else:
            K_error[i] = std(K_max - K_max_basic)

        is_time_far[i] = t_max > duration
        should_be_the_same[i] = not(is_t_max_basic_zero[i]) and not(is_time_far[i])

        if (t_error[i] > 3* 10**-4 or V_error[i] > 3 * 10**-3 or K_error[i] > 10**-3): #and should_be_the_same[i]:
            print 'ERROR spotted with same t_max:'

            print 'V error: {0}'.format(V_error[i])
            print 'K error: {0}'.format(K_error[i])
            wh = np.where((K_max - K_max_basic)**2 > 10**-15)

            print 'V_max: {0}, V_max_basic: {1}'.format(V_max,V_max_basic)
            print 'times: {2}, K_max: {0}, K_max_basic: {1}'.format(K_max[wh],K_max_basic[wh],x[wh])

            N_errors += 1

            pass

    print 'Error found: {0}'.format(N_errors)
    print 'ERROR MAX when should be the same:'
    print argmax(t_error[should_be_the_same]),max(t_error[should_be_the_same])
    print argmax(V_error[should_be_the_same]),max(V_error[should_be_the_same])
    print argmax(K_error[should_be_the_same]),max(K_error[should_be_the_same])

def test_find_max():

    random.seed(1)

    tau = 10 * 10**-3
    tau_s = tau / 4

    n = 100
    p = 2 * n
    n_spikes = 1

    N_iterations= 1
    N_shuffle = 200

    mu= 0.99

    Vthr = 1.
    V_0 = 2.12

    duration = .1
    delta_V_thr = 0.


    al_opt = 3. * 10**-3 * duration / (tau * n * V_0)

    X1 = create_random_dataset(n,0 ,duration ,n_spikes,p)

    tempo = TempotronExact(n,tau,tau_s,N_iterations,N_shuffle,al_opt,Vthr,V_0,delta_V_thr,mu,verbose=2)

    basic_vs_exact_error_check(tempo,X1)
    #exact_V_error_check(tempo,X1)

def test_train():

    tau_list = np.array([10.])*10**-3

    n = 500
    p = 2 * n
    n_spikes = 1

    N_iterations= 1
    N_shuffle = 20

    mu= 0.99

    Vthr = 1.
    V_0 = 2.12

    duration = .5
    delta_V_thr = 0.


    al_opt_list = 3.* np.array([10**-3])

    X = create_random_dataset(n,0 ,duration ,n_spikes,p)
    Y = np.hstack((np.zeros(p/2),np.ones(p-p/2)))

    for al_opt in al_opt_list:
        for tau in tau_list:
            tau_s = tau / 4
            al_opt =  al_opt * duration / (tau * n * V_0)
            tempo = TempotronExact(n,tau,tau_s,N_iterations,N_shuffle,al_opt,Vthr,V_0,delta_V_thr,mu,verbose=2)
            #tempo.set_discrete_times(FS,FS,0)
            tempo.train(X,Y)
            Ypredict = tempo.predict_basic(X)

            print '{0} classification mistakes.'.format(np.sum(Y != Ypredict))
            print 'Load: {0}, n: {1}, iterations: {2}'.format(p/n,n,N_shuffle)


    for k in range(p):
        for k in range(p):
            X1 = np.array(X)[Ypredict != Y]
            #print 'Ypredicted: {0}, Y: {1}'.format(Ypredict[k],Ypredict[k])
            basic_vs_exact_error_check(tempo,X1,duration + tau,re_init=True,plot_anyway=False,FS=10**5)



def test_SVM():

    tau = 10 * 10**-3
    tau_s = tau / 4

    n = 500
    p =  2 * n
    n_spikes = 1

    N_trials = 1
    N_iterations= 1
    N_shuffle = 200

    mu= .9

    Vthr = 1.
    V_0 = 2.12

    duration = .5
    delta_V_thr = 0.1


    al_opt = .01

    X = create_random_dataset(n,0 ,duration ,n_spikes,p)
    Y = np.hstack((np.zeros(p/2),np.ones(p-p/2)))

    for kk in range(0,N_trials):
        tempo = TempotronSVM(n,tau,tau_s,N_iterations,N_shuffle,al_opt,Vthr,V_0,delta_V_thr,mu=mu,verbose=2)
        tempo.train(X,Y)

        print 'SVM tempotron:'
        print '{0} classification mistakes.'.format(np.sum(Y != tempo.predict_basic(X)))
        print 'Load: {0}, n: {1}, iterations: {2}'.format(p/n,n,N_shuffle)


    return tempo




#test_find_max()
test_train()
#test_SVM()





