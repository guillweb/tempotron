__author__ = 'root'


from basic import *
from brian2 import *
import numpy as np
from profilehooks import profile
from sklearn.linear_model import SGDClassifier
from sklearn.svm import LinearSVC
from sklearn.grid_search import GridSearchCV
import copy
from lib.ctype_lib import find_first_larger


class MaxFinder:
    def __init__(self,tempo):
        self.tempo = tempo

        self.tau = tempo.tau
        self.tau_s = tempo.tau_s
        self.V_0 = tempo.V_0
        self.V_thr = tempo.V_thr
        self.n = tempo.n

        self.sample = []
        self.n_spikes = 0
        self.spike_sorted = np.zeros((0)) 

        self.Q_tau = np.zeros((1,0))
        self.Q_tau_s = np.zeros((1,0))

        self.t_max_list = [] 
        self.V_max_list = [] 

    def init_sample_max_search(self,x,sample_id=-1,load_g=True):

        self.sample = x
        self.x_shape = np.shape(x)

        spike_list = x.flatten()
        self.n_spikes = len(spike_list)
        self.ind_sort = np.argsort(spike_list)
        self.spike_sorted = np.sort(spike_list)

        if load_g:
            self.load_g(sample_id)

    def compute_g(self):

        g_tau_i = self.V_0 *np.exp(self.sample/ self.tau)
        g_tau_s_i = self.V_0 *np.exp(self.sample /self.tau_s)

        g_tau = g_tau_i.flatten()[self.ind_sort] / self.V_0
        g_tau_s = g_tau_s_i.flatten()[self.ind_sort] / self.V_0

        return g_tau_i,g_tau_s_i,g_tau,g_tau_s

    def load_g(self,sample_id):

        self.g_tau = self.g_tau_DB[sample_id,:]
        self.g_tau_s = self.g_tau_s_DB[sample_id,:]

        if self.x_shape[1] == 1:
            self.g_tau_i = self.g_tau_i_DB[sample_id,:]
            self.g_tau_s_i  = self.g_tau_s_i_DB[sample_id,:]
        else:
            self.g_tau_i = self.g_tau_i_DB[sample_id,:,:]
            self.g_tau_s_i = self.g_tau_s_i_DB[sample_id,:,:]

    def build_g_tau_DB(self,X):

        self.init_sample_max_search(X[0],load_g=False)
        N = len(X)

        self.g_tau_DB = np.zeros((N,self.n_spikes))
        self.g_tau_s_DB = np.zeros((N,self.n_spikes))

        if self.x_shape[1] == 1:
            self.g_tau_i_DB = np.zeros((N,self.n_spikes))
            self.g_tau_s_i_DB = np.zeros((N,self.n_spikes))
        else:
            self.g_tau_i_DB = np.zeros((N,self.x_shape[0],self.x_shape[1]))
            self.g_tau_s_i_DB = np.zeros((N,self.x_shape[0],self.x_shape[1]))


        for k in range(0,N):
            x = X[k]
            self.init_sample_max_search(x,load_g=False)
            g_tau_i,g_tau_s_i,g_tau,g_tau_s = self.compute_g()
            self.g_tau_DB[k,:] = g_tau
            self.g_tau_s_DB[k,:] = g_tau_s

            if self.x_shape[1] == 1:
                self.g_tau_i_DB[k,:] = g_tau_i.flatten()
                self.g_tau_s_i_DB[k,:] = g_tau_s_i.flatten()
            else:
                self.g_tau_i_DB[k,:,:] = g_tau_i
                self.g_tau_s_i_DB[k,:,:] = g_tau_s_i

    def update_Q(self,w):

        if self.x_shape[1] == 1:
            self.Q_tau = np.cumsum((w*self.g_tau_i)[self.ind_sort])
            self.Q_tau_s = np.cumsum((w*self.g_tau_s_i)[self.ind_sort])
        else:
            wg_tau = np.einsum('i,ik->ik',w,self.g_tau_i).flatten()
            wg_tau_s = np.einsum('i,ik->ik',w,self.g_tau_s_i).flatten()

            self.Q_tau = np.cumsum(wg_tau[self.ind_sort])
            self.Q_tau_s = np.cumsum(wg_tau_s[self.ind_sort])

    def max_search(self,n_spk):

        ind_max = np.argmax(self.V_max_list[0:n_spk])
        t_max = self.t_max_list[ind_max]
        V_max = self.V_max_list[ind_max]

        return ind_max,t_max,V_max

    def find_max(self,w):
        self.update_max_list(w)

        len_max_list = len(self.V_max_list)
        n_spk = find_first_larger(self.V_thr,self.V_max_list,len_max_list)+1

        # Case Where either, the threshold is not reached, or where the last one reaches it.
        if n_spk == len_max_list:
            ind_max,t_max,V_max = self.max_search(n_spk)
        # The threshold is reached before the last spike at spike n_spk-1, this one will lead to t_max
        elif n_spk < len_max_list:
            self.update_V_star_as_last(n_spk-1,w)
            t_max = self.t_max_list[n_spk-1]
            V_max = self.t_max_list[n_spk-1]
            ind_max = n_spk-1

        if self.x_shape[1] == 1:
            K_max = np.zeros(self.n_spikes)
            # for i in range(ind_max+1):
            #     K_max[self.ind_sort[i]] += self.tempo.K_val(w,self.spike_sorted[i],t_max)

            ind_to_keep = self.ind_sort[0:n_spk]
            s_tau = self.g_tau_i[ind_to_keep]
            s_tau_s = self.g_tau_s_i[ind_to_keep]
            K_max[ind_to_keep] = np.exp(- t_max / self.tau) * s_tau \
               - np.exp(- t_max / self.tau_s) * s_tau_s
        else:
            pass
            # ind_to_keep = self.sample <= t_max
            # s_tau = np.sum(self.g_tau_i[ind_to_keep],axis=1)
            # s_tau_s = np.sum(self.g_tau_s_i[ind_to_keep],axis=1)
            # K_max = np.exp(- t_max / self.tau) * s_tau \
            #     - np.exp(- t_max / self.tau_s) * s_tau_s

        return t_max,V_max,K_max,ind_max

    def update_max_list(self,w):
        self.update_Q(w)

        self.V_max_list = self.Q_tau / self.g_tau - self.Q_tau_s / self.g_tau_s


        n_spk = find_first_larger(self.V_thr,self.V_max_list,self.n_spikes)+1
        # If the threshold is reached at tk it means that it was reached just before. tk-1 reaches threshold.
        n_spk_eff = n_spk-1
        # Though it might be also before.
        if n_spk == 1:
            print 'ERROR: Impossible to be already at threshold at t0 before computation of t_star.'
            raise ValueError()

        self.V_max_list = self.V_max_list[0:n_spk_eff]
        self.t_max_list = self.spike_sorted[0:n_spk_eff].copy()


        Qtau = self.Q_tau[0:n_spk_eff-1]
        Qtau_s = self.Q_tau_s[0:n_spk_eff-1]

        # Check which are decreasing at t_k+1, last spike studied appart
        ind_decr = Qtau_s / ( self.g_tau_s[1:n_spk_eff] * self.tau_s) < Qtau / (self.g_tau[1:n_spk_eff] * self.tau)
        # Check increasing at t_k
        ind_incr = Qtau_s / ( self.g_tau_s[0:n_spk_eff-1] * self.tau_s) > Qtau / (self.g_tau[0:n_spk_eff-1] * self.tau)

        ind = np.logical_and(ind_decr,ind_incr)

        if ind.any():
            Qtau = Qtau[ind]
            Qtau_s = Qtau_s[ind]

            ratio = self.tau * Qtau_s / (self.tau_s * Qtau)
            t_star = np.log(ratio) * self.tau * self.tau_s / (self.tau - self.tau_s)

            V_star = np.exp(- t_star/ self.tau) * Qtau - np.exp(- t_star / self.tau_s) * Qtau_s

            self.V_max_list[ind] = V_star
            self.t_max_list[ind] = t_star

        self.update_V_star_as_last(n_spk_eff-1,w)

    def update_V_star_as_last(self,ind_spk,w):

        # Check le the last separatly because their are no spike after.
        last_ratio = self.tau * self.Q_tau_s[ind_spk] / (self.tau_s * self.Q_tau[ind_spk])
        last_not_before = last_ratio >= self.g_tau_s[ind_spk] / self.g_tau[ind_spk]
        is_last_increasing = self.Q_tau_s[ind_spk] / ( self.g_tau_s[ind_spk] * self.tau_s) > self.Q_tau[ind_spk] / (self.g_tau[ind_spk] * self.tau)

        if is_last_increasing and last_ratio >0  and last_not_before:

            t_star_end = np.log(last_ratio) * self.tau * self.tau_s / (self.tau - self.tau_s)
            self.t_max_list[ind_spk] = t_star_end


            # K_max = np.zeros(self.n_spikes)
            # for i in range(ind_spk+1):
            #     K_max[self.ind_sort[i]] += self.tempo.K_val(w,self.spike_sorted[i],t_star_end)
            # #V_star_end =  np.dot(w,K_max)
            V_star_end = np.exp(- t_star_end / self.tau) * self.Q_tau[ind_spk] - np.exp(- t_star_end / self.tau_s) * self.Q_tau_s[ind_spk]
            self.V_max_list[ind_spk] = V_star_end





class TempotronExact(Tempotron):

    def init_max_finder(self,X):
        self.max_finder = MaxFinder(self)
        self.max_finder.build_g_tau_DB(X)

    def train_one_exact(self,x,y,sample_id):

        pol = y*2-1


        # Compute max. Complexity n_spikes x n
        self.max_finder.init_sample_max_search(x,sample_id=sample_id)

        for i in range(0,self.N_iterations):
            t_max,V_max,K_max,ind_max = self.max_finder.find_max(self.w)
            #print 'Max of K_max: {0}'.format(np.max(K_max[K_max >0]))


            if not(self.is_error(V_max,pol)):
                break


            # Move toward the gradient
            dw = pol * self.al_opt * K_max
            self.iterate_weight(dw)

    def iterate_weight(self,dw,epsilon=10**-3):
        ind = np.abs(dw)/max(np.max(np.abs(dw)),epsilon) > epsilon
        if ind.any():
            dw[ind] += self.dw_previous[ind] * self.mu
            self.w[ind] += dw[ind]
            self.dw_previous[ind] = dw[ind]

    @profile
    def train(self,X,Y,print_every=10):

        N = len(X)
        self.init_max_finder(X)

        for k in range(0,self.N_shuffle):
            if mod(k,print_every) ==0 and self.verbose >= 2:
                print 'Step {0}, mistakes: {1}/{2}'.format(k, np.sum(self.predict_basic(X) != Y),N)
            for ind_x in range(0,N):
                    self.train_one_exact(X[ind_x],Y[ind_x],ind_x)

    def predict_basic(self,X,re_init_max_finder=False):

        N = len(X)
        Y = np.zeros(N)

        if re_init_max_finder:
            self.init_max_finder(X)

        for k in range(0,N):
            x = X[k]
            self.max_finder.init_sample_max_search(x,sample_id=k)
            t_max,V_max,K_max,ind_max = self.max_finder.find_max(self.w)

            Y[k] = V_max > self.V_thr


        return Y




class TempotronSVM(TempotronExact):

    @profile
    def train(self,X,Y):

        Y_eff = (Y*2-1).flatten()

        params = np.power(10.,[4])
        N = len(X)
        self.init_max_finder(X)
        self.clf = LinearSVC()

        X_max = np.zeros((self.n,0))
        for i in range(0,self.N_shuffle):
            X_max = np.zeros((self.n,N))

            for k in range(0,N):
                x = X[k]
                self.max_finder.init_sample_max_search(x,sample_id=k)
                t_max,V_max,K_max,ind_max = self.max_finder.find_max(self.w)
                X_max[:,k] = K_max / self.V_thr


            mistakes = np.sum( np.int_(Y_eff>0) != np.int_(np.dot(self.w,X_max * self.V_thr) > self.V_thr))
            if self.verbose >=2:
                print 'Step {2}. Mistakes before SVM: {0}/{1}'.format(mistakes,N,i)
                #print np.int_((np.dot(w_eff,X_eff) + b_eff) > 0)

            if mistakes == 0:
                if self.verbose >= 2:
                    print 'Solution found.'
                    Y_predict = self.tempo.predict_basic(X)
                    Y_sol = np.int_(Y_eff[0:N]>0)

                    print 'Final mistakes: {0}'.format( np.sum( Y_predict != Y_sol ) )
                break




            al = self.delta_V_thr / self.V_thr

            std0 = np.std(X_max,axis=1)
            X_eff = np.dot( np.diag(1/std0), X_max )

            best_error = N
            for C in params:
                self.clf = LinearSVC(C=C)
                self.clf.fit(X_eff.T,Y_eff)

                b_eff_svm = self.clf.intercept_
                w_eff_svm = self.clf.coef_

                error = np.sum(Y_eff * (np.dot(w_eff_svm,X_eff) + b_eff_svm) < 1)
                if error < best_error:
                    b_eff = b_eff_svm
                    w_eff = w_eff_svm
                    best_error = error





            if b_eff >= 0:
                b_eff *= -1
                w_eff *= -1
                Y_eff *= -1
                self.w *= -1
                print 'b_eff >0. No solution with positive threshold. Rotating problem.'

            al = -1/b_eff
            w = al * w_eff * 1/std0

            dw = self.al_opt * (w - self.w)
            self.iterate_weight(dw.flatten())

            if self.verbose >= 2:
                print 'Mistakes solution SVM: {0}/{1}'.format(np.sum( np.int_(Y_eff>0) != np.int_(np.dot(w,X_max * self.V_thr) > self.V_thr)),N)
                print 'Mistakes with combined weights SVM: {0}/{1}'.format(np.sum( np.int_(Y_eff>0) != np.int_(np.dot(self.w,X_max * self.V_thr) > self.V_thr)),N)
                print '---------------------'


        return w_eff,b_eff