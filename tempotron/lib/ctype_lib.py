__author__ = 'root'

import numpy as np

import ctypes
lib = ctypes.CDLL('../../lib/find_first_larger.dylib')
lib.find_first_larger.restype = ctypes.c_long
lib.find_first_larger.argtypes = (ctypes.c_double, ctypes.POINTER(ctypes.c_double), ctypes.c_long)

def find_first_larger(V_thr,V_max_list,n_spikes):
    return lib.find_first_larger(V_thr, V_max_list.ctypes.data_as(ctypes.POINTER(ctypes.c_double)),n_spikes)
