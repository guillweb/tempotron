__author__ = 'root'

from basic import *
import numpy as np


def test_train_one():

    FS = 10**4

    N = 2
    x = [[1,20,90],[25,90]]
    y = 1

    tempo = TempotronBasic(N)
    tempo.set_discrete_times(FS,FS)
    tempo.train_one_basic(x,y)


def test_datasets():
    print 'random:'
    print create_random_dataset(3,0,1,3,2)

    print 'coincident:'
    print create_coincident_dataset(3,0,1,3,2)



def test_rapid_convolution():

    FS = 10**4

    spike_times = create_random_dataset(3,0,1,10,1)

    tempo = TempotronBasic(3)
    tempo.set_discrete_times(FS,FS)

    t = np.random.rand(5)
    t_ind = np.int_(t * FS)


    a = tempo.spikes_K(spike_times[0])[:,t_ind]
    b = tempo.sp_K(spike_times[0],t)

    print "K approximation error:"
    print np.var(a - b)**2






def test_train_coincidence():

    #FS = 10**3

    tau_s = 3. * 10**-3
    tau = 4 * tau_s

    N = 5
    K= 10
    n_spikes = 12

    N_iterations= 100
    N_shuffle = 10

    al_opt = 10**-2

    Vthr = 1.
    V_0 = 1.
    delta_V_thr = 0.2

    X1 = create_random_dataset(N,0,1,n_spikes,K)
    X2 = create_coincident_dataset(N,0,1,n_spikes,K)

    X = X1 + X2
    Y = np.hstack((np.zeros(K),np.ones(K)))

    tempo = TempotronBasic(N,tau,tau_s,N_iterations,N_shuffle,al_opt,Vthr,V_0,delta_V_thr)
    #tempo.set_discrete_times(FS,FS,0)
    tempo.train(X,Y)

    print Y
    print tempo.predict_basic(X)

    return tempo

#test_train_one()
#test_datasets()
#test_train_coincidence()
#test_rapid_convolution()
